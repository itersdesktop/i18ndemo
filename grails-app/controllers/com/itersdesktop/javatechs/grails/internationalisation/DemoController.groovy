package com.itersdesktop.javatechs.grails.internationalisation

class DemoController {
    def index() {
        render(view: "index")
    }
}