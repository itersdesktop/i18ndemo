<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>Demo | BioModels</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="layout" content="main"/>
    <script>

    </script>
    <g:set var="locale" value="${session.'org.springframework.web.servlet.i18n.SessionLocaleResolver.LOCALE'}"/>
    <asset:i18n locale="${locale}"/>
</head>

<body>
<h2><g:message code="welcome.message"/> </h2>
</body>
</html>